package pl.home.vetclinic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.home.vetclinic.domain.Owner;

@Repository
public interface OwnerRepository extends PagingAndSortingRepository<Owner, Long> {
}
