package pl.home.vetclinic.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.home.vetclinic.domain.Animal;
import pl.home.vetclinic.domain.Owner;

@Repository
public interface AnimalRepository extends PagingAndSortingRepository<Animal, Long> {
    long countByOwner(Owner owner);
}
