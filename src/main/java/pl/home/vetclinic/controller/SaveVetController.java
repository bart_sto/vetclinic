package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.home.vetclinic.domain.Vet;
import pl.home.vetclinic.repository.VetRepository;

@Controller
public class SaveVetController {

    @Autowired
    private VetRepository vetRepository;


    @PostMapping("/vets/saveNew")
    public String saveNewVet(@ModelAttribute("newV") @Validated Vet vet,
                                     BindingResult br) {
        if (br.hasErrors()) {
            return "newVet";
        }
        else {
            vetRepository.save(vet);

            return "redirect:/vets";
        }
    }

    @PostMapping("/vets/saveEdited/{idVet}")
    public String saveNewVet(@PathVariable("idVet") Long idVet,
                                     @Validated  Vet vet,
                                     BindingResult br, Model model) {
        if (br.hasErrors()) {
            model.addAttribute("editedVet", vet);
            return "editVet";
        } else {
            vet.setIdVet(idVet);
            vetRepository.save(vet);

            return "redirect:/vets";
        }
    }
}
