package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.home.vetclinic.domain.Owner;
import pl.home.vetclinic.repository.AnimalRepository;
import pl.home.vetclinic.repository.OwnerRepository;

import java.security.Principal;

@Controller
@RequestMapping("/owners")
public class ListOfOwnersController {

    @Autowired
    private OwnerRepository ownerRepository;

    @Autowired
    private AnimalRepository animalRepository;

    @GetMapping
    public String getListOfOwners(Model model, Principal principal) {
        Iterable<Owner> owners = ownerRepository.findAll();
        for(Owner owner : owners) {
            long numberOfAnimals = animalRepository.countByOwner(owner);
            owner.setNumberOfAnimals(numberOfAnimals);
        }
        model.addAttribute("listOfOwners", owners);
        return "listOfOwners";
    }

    @GetMapping("/add")
    public String addNewOwner(Model model){
        Owner owner = new Owner();

        model.addAttribute("newO", owner);

        return "newOwner";
    }

    @GetMapping("/delete/{idOwner}")
    public String deleteOwner(@PathVariable("idOwner") Long idOwner) {
        Owner owner = ownerRepository.findOne(idOwner);

        if(owner != null) {
            Long numberOfAnimals = animalRepository.countByOwner(owner);

            if (numberOfAnimals == 0) {
                ownerRepository.delete(owner);

                return "redirect:/owners";
            } else {
                return "notEmptyOwner";
            }
        } else {
            return "ownerDoesNotExist";
        }
    }
}
