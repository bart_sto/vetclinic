package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.home.vetclinic.domain.Animal;
import pl.home.vetclinic.domain.Owner;
import pl.home.vetclinic.repository.AnimalRepository;
import pl.home.vetclinic.repository.OwnerRepository;

@Controller
@RequestMapping("/animals")
public class ListOfAnimalsController {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @GetMapping
    public String listOfAnimals(Model model, Pageable pageable){
        model.addAttribute("listOfAnimals", animalRepository.findAll(pageable));

        return "listOfAnimals";
    }

    @GetMapping("/add")
    public String addNewAnimal(Model model){
        Animal animal = new Animal();

        model.addAttribute("newA", animal);
        model.addAttribute("listOfOwners" , ownerRepository.findAll());

        return "newAnimal";
    }

    @GetMapping("/edit/{idAnimal}")
    public String editAnimal(@PathVariable("idAnimal") Long idAnimal, Model model){
        Animal animal = animalRepository.findOne(idAnimal);

        if (animal.getOwner() != null) {
            animal.setIdOwner(animal.getOwner().getIdOwner());
        }

        model.addAttribute("editedAnimal", animal);
        model.addAttribute("listOfOwners", ownerRepository.findAll());

        return "editAnimal";
    }

    @GetMapping("/delete/{idAnimal}")
    public String deleteAnimal(@PathVariable("idAnimal") Long idAnimal){
        Animal animal = animalRepository.findOne(idAnimal);
        if(animal != null){
            animalRepository.delete(idAnimal);
            return "redirect:/animals";
        } else {
            return "animalDoesNotExist";
        }
    }

}
