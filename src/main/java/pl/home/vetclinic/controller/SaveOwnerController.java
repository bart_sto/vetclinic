package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.home.vetclinic.domain.Owner;
import pl.home.vetclinic.repository.OwnerRepository;


@Controller
public class SaveOwnerController {

    @Autowired
    private OwnerRepository ownerRepository;

    @PostMapping("/owners/saveNew")
    public String saveNewOwner(@ModelAttribute("newO") @Validated Owner owner,
                                BindingResult br) {
        if (br.hasErrors()) {
            return "newOwner";
        } else {
            ownerRepository.save(owner);

            return "redirect:/owners";
        }
    }

    @PostMapping("/owners/saveEdited/{idVet}")
    public String saveNewOwner(@PathVariable("idOwner") Long idOwner,
                                @Validated Owner owner,
                                BindingResult br, Model model){
        if (br.hasErrors()) {
            model.addAttribute("editedOwner", owner);
            return "editOwner";
        } else {
            owner.setIdOwner(idOwner);
            ownerRepository.save(owner);

            return "redirect:/owners";
        }

    }
}
