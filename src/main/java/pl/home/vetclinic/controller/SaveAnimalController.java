package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.home.vetclinic.domain.Animal;
import pl.home.vetclinic.domain.Owner;
import pl.home.vetclinic.repository.AnimalRepository;
import pl.home.vetclinic.repository.OwnerRepository;

@Controller
public class SaveAnimalController {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private OwnerRepository ownerRepository;

    @ModelAttribute("listOfOwners")
    public Iterable<Owner> allOwners() {
        return ownerRepository.findAll();
    }

    @PostMapping("/animals/saveNew")
    public String saveNewAnimal(@ModelAttribute("newA") @Validated Animal animal,
                                BindingResult br) {
        if (br.hasErrors()) {
            return "newAnimal";
        } else {
            if (animal.getIdOwner() != null) {
                Owner owner = ownerRepository.findOne(animal.getIdOwner());
                animal.setOwner(owner);
            }
            animalRepository.save(animal);

            return "redirect:/animals";
        }
    }

    @PostMapping("/animals/saveEdited/{idAnimal}")
    public String saveNewAnimal(@PathVariable("idAnimal") Long idAnimal,
                                @Validated Animal animal,
                                BindingResult br, Model model){
        if (br.hasErrors()) {
            model.addAttribute("editedAnimal", animal);
            return "editAnimal";
        } else {
            if (animal.getIdOwner() != null) {
                animal.setOwner(ownerRepository.findOne(animal.getIdOwner()));
            }
            animal.setIdAnimal(idAnimal);
            animalRepository.save(animal);

            return "redirect:/animals";
        }

    }
}
