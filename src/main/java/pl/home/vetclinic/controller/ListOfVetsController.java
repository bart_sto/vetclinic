package pl.home.vetclinic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.home.vetclinic.domain.Vet;
import pl.home.vetclinic.repository.VetRepository;

import org.springframework.data.domain.Pageable;

@Controller
@RequestMapping("/vets")
public class ListOfVetsController {

    @Autowired
    private VetRepository vetRepository;

    @GetMapping
    public String listOfVets(Model model, Pageable pageable){
        model.addAttribute("listOfVets", vetRepository.findAll(pageable));

        return "listOfVets";
    }

    @GetMapping("/add")
    public String addNewVet(Model model){
        Vet vet = new Vet();

        model.addAttribute("newV", vet);

        return "newVet";
    }

    @GetMapping("/edit/{idVet}")
    public String editVet(@PathVariable("idVet") Long idVet,
                               Model model) {
        Vet vet = vetRepository.findOne(idVet);

        model.addAttribute("editedVet",vet);

        return "editVet";
    }

    @GetMapping("/delete/{idVet}")
    public String deleteVet(@PathVariable("idVet") Long idVet) {
        Vet vet = vetRepository.findOne(idVet);
        if (vet != null) {
            vetRepository.delete(idVet);

            return "redirect:/vets";
        } else {
            return "vetDoesNotExist";
        }
    }

}
