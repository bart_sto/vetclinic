package pl.home.vetclinic.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAnimal;

    @NotEmpty
    private String name;

    @NotEmpty
    private String category;

    @NotEmpty
    private String breed;

    @NotNull
    private Integer birthDate;

    @ManyToOne
    private Owner owner;

    @Transient
    private Long idOwner;


}
